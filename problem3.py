#!/usr/bin/python


def get_div(num):
	for x in range(3,int(pow(num, 0.5)),2):
		o = str(num / x)
		if o.find('.') == -1:
			return x
		if o.find('.') > 0:
			if int(o[o.find('.')+1:]) == 0:
				return x

def max_val(v):
	max = 1
	for i in v:
		if i > max:
			max = i
	return max

def get_divs(num):
	f = [1]
	while max(f) < pow(num, 0.5):
		d = get_div(num)
		f.append(d)
		num = num / d
	return f

def main():
	num = 600851475143
	print ('f = ', get_divs(num))

if __name__ == '__main__':
	main()

