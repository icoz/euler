#!/usr/bin/python


def gen_fib(count=None, max=None):
	a, b = (1, 2)
	yield 1
	yield 2
	if count is not None:
		for i in range(2, count):
			c = a + b
			a = b
			b = c
			yield c
		return
	if max is not None:
		while True:
			c = a + b
			if c > max:
				return
			a = b
			b = c
			yield c

for num in gen_fib(count=10):
	print(num)
for num in gen_fib(max=1000):
	print(num)
sum = 0
for num in gen_fib(max=4000000):
	if num % 2 == 0:
		sum = sum + num
print(sum)