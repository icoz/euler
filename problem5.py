#!/usr/bin/python

def get_div(num):
    for x in range(2,num,1):
        o = str(num / x)
        # print (o)
        if o.find('.') == -1:
            return x
        if o.find('.') > 0:
            if int(o[o.find('.')+1:]) == 0:
                return x

def get_divs(num):
    f = [1]
    while num != 1:
        d = get_div(num)
        if d is None:
            f.append(num)
            return f
        f.append(d)
        num = int(num / d)
    return f

def count_divs(div_list):
    div = dict()
    for n in div_list:
        try:
            div[n] = div[n] + 1
        except KeyError:
            div[n] = 1
    return div
    pass

def mul_divs(div_dict):
    mul = 1
    for num in div_dict:
        mul = mul * pow(num, div_dict[num])
    return mul

def check_num(num):
    for i in range(20):
        n = i+1
        print("%s / %s = %s"%(num, n, num / n))
    f = get_divs(num)
    print(f)
    print(count_divs(f))

def main():
    d = dict()
    li = list()
    for i in range(2,21):
        f = get_divs(i)
        # print('i=%i f=' % i, f)
        cf = count_divs(f)
        li.append(cf)
        if i !=  mul_divs(cf):
            print(i, cf, mul_divs(cf))
    for cf in li:
        for n in cf:
            try:
                if cf[n] > d[n]:
                    d[n] = cf[n]
            except KeyError:
                d[n] = cf[n]
    print('d = ', d)
    print('val = ',mul_divs(d))
    check_num(mul_divs(d))
    pass

if __name__ == '__main__':
    main()